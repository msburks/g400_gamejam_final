﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    int n;
    public GameObject clawImage;
    public GameObject lizardImage;
    public GameObject cheetahImage;
    public GameObject frogImage;
    public GameObject fishImage;
    public GameObject wingsImage;

    //booleans to start the game
    public bool feetChoice;
    public bool armsChoice;
    public bool extraChoice;


    // Start is called before the first frame update
    void Start()
    {
        clawImage.SetActive(false);
        lizardImage.SetActive(false);
        cheetahImage.SetActive(false);
        frogImage.SetActive(false);
        fishImage.SetActive(false);
        wingsImage.SetActive(false);


        feetChoice = false;
        armsChoice = false;
        extraChoice = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonPress()
    {
        n++;
        Debug.Log("Button clicked " + n + " times.");
    }

    //button to toggle claw image in character creation screen
    public void ClawButton()
    {
        Debug.Log("Button has been hit!");
        clawImage.SetActive(true);
        lizardImage.SetActive(false);

        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/UI/mole-dig");

        //set booleans for charater selection in GC

        GameController.gc.clawArms = true;
        GameController.gc.lizardArms = false;
        //sets that the player made a choice of arms to true
        armsChoice = true;

    }

    //button to toggle lizard image in character creation screen
    public void LizardButton()
    {
        Debug.Log("Button has been hit!");
        lizardImage.SetActive(true);
        clawImage.SetActive(false);

        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/UI/gekko");

        //set booleans for charater selection in GC
        GameController.gc.clawArms = false;
        GameController.gc.lizardArms = true;
        //sets that the player made a choice of arms to true
        armsChoice = true;

    }

    //button to toggle cheetah image in character creation screen
    public void CheetahButton()
    {
        Debug.Log("Button has been hit!");
        cheetahImage.SetActive(true);
        frogImage.SetActive(false);

        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/UI/Cheetah Sound Effect");

        //set booleans for charater selection in GC
        GameController.gc.cheetahFeet = true;
        GameController.gc.frogFeet = false;
        //sets that the player made a choice of feet to true
        feetChoice = true;

    }

    //button to toggle frog image in character creation screen
    public void FrogButton()
    {
        Debug.Log("Button has been hit!");
        frogImage.SetActive(true);
        cheetahImage.SetActive(false);

        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/UI/frog-croaking-sound-effect edited");

        //set booleans for charater selection in GC
        GameController.gc.cheetahFeet = false;
        GameController.gc.frogFeet = true;
        //sets that the player made a choice of feet to true
        feetChoice = true;
    }

    //button to toggle fish image in character creation screen
    public void FishButton()
    {
        Debug.Log("Button has been hit!");
        fishImage.SetActive(true);
        wingsImage.SetActive(false);

        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/UI/fish");

        //set booleans for charater selection in GC
        GameController.gc.fishExtra = true;
        GameController.gc.wingsExtra = false;
        //sets that the player made a choice of extra to true
        extraChoice = true;
    }

    //button to toggle wings image in character creation screen
    public void WingsButton()
    {
        Debug.Log("Button has been hit!");
        wingsImage.SetActive(true);
        fishImage.SetActive(false);

        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/UI/bird");

        //set booleans for charater selection in GC
        GameController.gc.fishExtra = false;
        GameController.gc.wingsExtra = true;
        //sets that the player made a choice of extra to true
        extraChoice = true;
    }

    public void StartButton()
    {
        if (armsChoice && feetChoice && extraChoice == true)
        {
            SceneManager.LoadScene("Instructions");
        }
    }
}
