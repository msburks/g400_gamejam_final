﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HudController : MonoBehaviour
{
    
    public GameObject clawImage;
    public GameObject lizardImage;
    public GameObject cheetahImage;
    public GameObject frogImage;
    public GameObject fishImage;
    public GameObject wingsImage;



    //Winning stuff


    // Start is called before the first frame update
    void Awake()
    {
        clawImage.SetActive(false);
        lizardImage.SetActive(false);
        cheetahImage.SetActive(false);
        frogImage.SetActive(false);
        fishImage.SetActive(false);
        wingsImage.SetActive(false);
    }

    public void Start()
    {
        PlayerSetup();
    }

    public void PlayerSetup()
    {
        if (GameController.gc.cheetahFeet == true)
        {
            cheetahImage.SetActive(true);
        }
        else
        {
            frogImage.SetActive(true);
        }

        if (GameController.gc.clawArms == true)
        {
            clawImage.SetActive(true);
        }
        else
        {
            lizardImage.SetActive(true);
        }
        if (GameController.gc.fishExtra == true)
        {
            fishImage.SetActive(true);
        }
        else
        {
            wingsImage.SetActive(true);
        }
    }

    
}
