﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InstructionScript : MonoBehaviour
{
    public GameObject clawImage;
    public GameObject lizardImage;
    public GameObject cheetahImage;
    public GameObject frogImage;
    public GameObject fishImage;
    public GameObject wingsImage;

    
    public static bool music;

    // Start is called before the first frame update
    void Start()
    {
        clawImage.SetActive(false);
        lizardImage.SetActive(false);
        cheetahImage.SetActive(false);
        frogImage.SetActive(false);
        fishImage.SetActive(false);
        wingsImage.SetActive(false);


        PlayerSetup();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        music = true; //tells music to shift to course music

        SceneManager.LoadScene("OC Scene");
    }

    public void PlayerSetup()
    {
        if (GameController.gc.cheetahFeet == true)
        {
            cheetahImage.SetActive(true);
        }
        else
        {
            frogImage.SetActive(true);
        }

        if (GameController.gc.clawArms == true)
        {
            clawImage.SetActive(true);
        }
        else
        {
            lizardImage.SetActive(true);
        }
        if (GameController.gc.fishExtra == true)
        {
            fishImage.SetActive(true);
        }
        else
        {
            wingsImage.SetActive(true);
        }
    }
}
