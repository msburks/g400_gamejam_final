﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall_Break : MonoBehaviour
{
    public GameObject Break;
    public GameObject Break1;
    public GameObject Break2;
    //public GameObject TB;


    //Winning Stuff


    public bool hasWon;
    public GameObject winText;



    // Start is called before the first frame update
    void Start()
    {
        winText.SetActive(false);
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("break") && GameController.gc.clawArms == true)
        {
            Break.SetActive(false);
            Break1.SetActive(false);
            Break2.SetActive(false);
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Abilities/break through");
        }

        if (other.gameObject.CompareTag("win"))
        {
            hasWon = true;
        }
    }



    bool bell = false;
    public static bool fin = false;

    // Update is called once per frame
    void Update()
    {
        if (hasWon == true)
        {
            winText.SetActive(true);
            if (bell == false)
            {
                fin = true;
                bell = true;
            }

            Pause();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Resume();
            }


        }
    }










    public void Pause()
    {
        Time.timeScale = 0f;
    }
    public void Resume()
    {
        Time.timeScale = 1f;

    }
}
