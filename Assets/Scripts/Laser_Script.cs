﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Laser_Script : MonoBehaviour
{
    //public GameObject laser;


    // Start is called before the first frame update
    void Start()
    {
    //    PlayerSetup_2();
    }

    public void PlayerSetup_2()
    {
        if (GameController.gc.lizardArms == true)
       {
            //laser.SetActive(false);
            //laser.collider.enabled = false;
            //GetComponent<Collider>().enabled = false;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("laser") && GameController.gc.lizardArms == false)
        {
            SceneManager.LoadScene("OC Scene");
        }

        if (other.gameObject.CompareTag("threshold"))
        {
            SceneManager.LoadScene("OC Scene");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
