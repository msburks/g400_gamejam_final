﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class CountdownTimer : MonoBehaviour
{
    float currentTime = 0f;
    float startingTime = 50f;

    [SerializeField] TextMeshProUGUI TimerText;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = startingTime;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        TimerText.text = currentTime.ToString("0.00");

        if (currentTime <= 50)
        {
            TimerText.color = Color.green;
        }

        if (currentTime <= 25)
        {
            TimerText.color = Color.yellow;
        }

        if (currentTime <= 10)
        {
            TimerText.color = Color.red;
        }

        if (currentTime <= 0)
        {
            currentTime = 0;
        }


        if (currentTime == 0)
        {
            SceneManager.LoadScene("OC Scene");
        }
    }
}

// https://www.youtube.com/watch?v=o0j7PdU88a4 //