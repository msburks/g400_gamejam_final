﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public FMOD.Studio.EventInstance gameMusic;
    

    // Start is called before the first frame update
    void Start()
    {
        gameMusic = FMODUnity.RuntimeManager.CreateInstance("event:/Music/Music");
        gameMusic.start();
    }

    // Update is called once per frame
    void Update()
    {
        if(InstructionScript.music == true)
        {
            Debug.Log("YES!");
            gameMusic.setParameterByName("Level Start", 1);

            InstructionScript.music = false;
        }

        if (Wall_Break.fin == true)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/End bell");
            gameMusic.setParameterByName("Underwater", 2);
            
            Wall_Break.fin = false;
        }
    }

    //when game is over, this will fade the music out entirely
    public void GameEnd()
    {
        gameMusic.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
